(function () {
	var options = {
		whatsapp: '254705011396',
		call_to_action: '',
		position: 'right',
		button_color: '#0e4358',
		pre_filled_message: 'Hello! BAYZINET',
		greeting_message: 'Hello, how may we help you? Just send us a message now to get assistance.'
	};
	var proto = document.location.protocol,
		host = 'bayzinet.co.ke',
		url = proto + '//scripts.' + host;
	var s = document.createElement('script');
	s.type = 'text/javascript';
	s.async = true;
	s.src = url + '/socials_plugin.js';
	s.onload = function () {
		WhWidgetSendButton.init(host, proto, options);
	};
	var x = document.getElementsByTagName('script')[0];
	x.parentNode.insertBefore(s, x);
})();
